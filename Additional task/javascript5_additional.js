// Реалізувати функцію створення об'єкта "юзер". Технічні вимоги:
// + Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// + При виклику функція повинна запитати ім'я та прізвище.
// + Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та LastName.
// + Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, 
// все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// + Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). 
// Вивести у консоль результат виконання функції.
// 
// Необов'язкове завдання підвищеної складності:
// - Зробити так, щоб властивості firstName та LastName не можна було змінювати напряму. 
// Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

"use strict";
function createNewUser() {
    const newUser = {};
    let firstName = "";
    let lastName = "";

    Object.defineProperty(newUser, firstName, {
        // value: "",
        // writable: false,
        get: function () {
            return firstName;
        }
    });
    // Object.defineProperty(newUser, lastName, {
    //     // value: "",
    //     // writable: false,
    //     get: function () {
    //         return lastName;
    //     }
    // });

    const askAge = confirm("are you already 18 years old?")
    if (askAge === false) {
        console.error('Sorry, you can not login. See you later.');
        alert('Sorry, you can not login. See you later.');
    } else {
        {
            newUser.firstName = prompt("Enter your name.");
            newUser.lastName = prompt("Enter your surname.");
        }
    };
    newUser.getLogin = function () { return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase() };
    return newUser;
};
let newUser1 = createNewUser();
console.log(newUser1.getLogin());




